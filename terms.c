#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char **argv) 
{
	char *slavename;
	int masterfd;
	int rrtn;
	char cmdstr[4096];
	struct stat buf;
	char *homedir = getenv("HOME");
	char pseudoDevName[128];
	unsigned char input[150];
	unsigned char output[4096];

	strncat(pseudoDevName, homedir, 110);
	strncat(pseudoDevName, "/pseudodev", 10);
	printf("lstat return for existing file: %d\n", lstat("/dev/cdrom", &buf));
	printf("lstat return for non-existing file: %d\n", lstat("/dev/fish", &buf));

	if(lstat( pseudoDevName, &buf))
	{
		printf("Return was non-zero");
		printf("Indicates tile did not exist");
	} else 
	{
		//
		// file exists
		printf("removing symlink \n");
		strncat(cmdstr, "rm ", 3);
		strncat(cmdstr, pseudoDevName, 128);
		printf("The command is this: %s\n", cmdstr);
		system(cmdstr);
		strncpy(cmdstr, "", 1); // clear
	}



	masterfd = open("/dev/ptmx", O_RDWR);
	grantpt(masterfd);
	unlockpt(masterfd);
	// slavename = ptsname(masterfd);

	printf("Slavename: %s\n", ptsname(masterfd));

	strncat(cmdstr, "ln -s ", 6);
	strncat(cmdstr, ptsname(masterfd), 32);
	strncat(cmdstr, " ", 1);
	strncat(cmdstr, pseudoDevName, 128);
	printf("Command string: %s\n", cmdstr);
	system(cmdstr);
	// system("ln -s slavename ~/pseudoterm");


	while(1)
	{
		rrtn = read(masterfd, output, sizeof(output));
		// printf("Read: [%s]\n", output);
		if(rrtn > 0)
		{
			printf("Read %d bytes of data.\n", rrtn);
			for(int i =0; i < rrtn; i++)
			{
				if((output[i] > 31) && (output[i] < 127) )
				{
					printf("[%04d]: 0x%02x, (%c)\n", i, output[i],(char) output[i]);
				} else {
					printf("[%04d]: 0x%02x, (unprintable)\n", i, output[i]);
				}
			}
			printf("Done with %d bytes.\n", rrtn);
		}
	}
}
