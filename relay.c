#define _XOPEN_SOURCE 600

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/stat.h>
#include <unistd.h>


void printHex(const char * text, int length)
{
	// printf("Received: %s\n", text);
	for(int i=0; i<length; i++)
	{
		if((text[i] > 31) && (text[i] < 127))
		{
			printf("[%04d]: 0x%02x, (%c)\n", i, (unsigned char) text[i], (char)text[i]);
		} else {
			printf("[%04d]: 0x%02x, (unprintable)\n", i, (unsigned char) text[i]);
		}
	}
}

int main(int argc, char **argv) 
{
	char *slavename;
	int masterfd;
	int phytty;
	int rrtn;
	int phyrrtn;
	int phywrite;
	int pseudowrite;
	unsigned char input[8];
	unsigned char output[8];
	unsigned char phyOutput[8];
	masterfd = open("/dev/ptmx", O_RDWR);
	phytty = open("/dev/ttyUSB0", O_RDWR);
	grantpt(masterfd);
	unlockpt(masterfd);
	// slavename = ptsname(masterfd);

	printf("Slavename: %s\n", ptsname(masterfd));
	printf("Master: %d, phy: %d\n", masterfd, phytty);
	while(1)
	{
		printf("Waiting on pseudo term\n");
		rrtn = read(masterfd, output, sizeof(output));
		printf("Waiting on physical term\n");
		phyrrtn = read(phytty, phyOutput, sizeof(phyOutput));

		// printf("Read: [%s]\n", output);
		if(rrtn > 0)
		{
			printf("Read %d bytes of data from pseudo-tty.\n", rrtn);
			printHex(output, rrtn);
			phywrite = write(phytty, output, rrtn);
			printf("Write %d bytes of data to phy-tty.\n", phywrite);
		}
		if(phyrrtn > 0)
		{
			printf("Read %d bytes of data from phy-tty.\n", phyrrtn);
			printHex(phyOutput, phyrrtn);
			pseudowrite = write(masterfd, phyOutput, phyrrtn);
			printf("Write %d bytes of data to pseudo-tty.\n", pseudowrite);
		}
	}
}
